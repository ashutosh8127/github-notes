import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';
import api from '../Uti/api';
import Profile from './Profile';
import Repositories from './Repositories';
import Notes from './Notes';



class Dashboard extends Component {
           makeBackground(btn) {
                var obj = {
              flexDirection: 'row',
             alignSelf: 'stretch',
              justifyContent: 'center',
               flex: 1
              };

              if (btn === 0) {
            obj.backgroundColor = '#48BBEC';
           } else if (btn === 1) {
            obj.backgroundColor = '#E77AAE';
             } else {
               obj.backgroundColor = '#758BF4';
                }

               return obj;
                }
                goToProfile() {
                 this.props.navigator.push({
                 title: 'Profile Page',
                  component: Profile,
                  passProps: { userInfo: this.props.userInfo }
                });
               }
                goToRepos() {
                    console.log(this.props.userInfo);
                    console.log(api);
                   api.getRepos(this.props.userInfo.login)
                   .then((res) => {
                    this.props.navigator.push({
                    title: 'Repos',
                    component: Repositories,
                    passProps: { userInfo: this.props.userInfo, repos: res }
                    });
                 });
               }
             goToNotes() {
                   api.getNotes(this.props.userInfo.login)
                   .then((res) => {
                   res = res || {};
                   this.props.navigator.push({
                    component: Notes,
                    title: 'Notes',
                     passProps: {
                        userInfo: this.props.userInfo,
                        notes: res
                       }
                   });
               });
              }

        render() {
        return ( 
            <View style = { styles.container } >
            <Image
          source={{uri: this.props.userInfo.avatar_url}}
          style={styles.image}
        />
        <TouchableHighlight
        style={this.makeBackground(0)}
        onPress={this.goToProfile.bind(this)}
         underlayColor='88D4F5'>  
          <Text style={styles.buttonText}>View Profile</Text>
          </TouchableHighlight>

          <TouchableHighlight
          style={this.makeBackground(1)}
        onPress={this.goToRepos.bind(this)}
         underlayColor='88D4F5'>  
          <Text style={styles.buttonText}>View Repos</Text>
          </TouchableHighlight>

          <TouchableHighlight
          style={this.makeBackground(2)}
        onPress={this.goToNotes.bind(this)}
         underlayColor='88D4F5'>  
          <Text style={styles.buttonText}>View Notes</Text>
          </TouchableHighlight>
			</View>
        );
    }
}

export default Dashboard;

const styles = StyleSheet.create({
    container: {
        flex: 1,

        marginTop: 65,

    },
    title: {
        marginBottom: 20,
        fontSize: 25,
        textAlign: 'center',
        color: '#fff'
    },
    searchInput: {
        height: 50,
        padding: 4,
        marginRight: 5,
        fontSize: 23,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 8,
        color: 'white'
    },
    buttonText: {
        fontSize: 24,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 45,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    image: {
        height: 350,
    },
});