var api = {
    getBio(username) {
        username = username.toLowerCase().trim();
        var url = `https://api.github.com/users/${username}`;
        return fetch(url).then((res) => res.json());
    },
    getRepos(username) {
    username = username.toLowerCase().trim();
    var url = `https://api.github.com/users/${username}/repos`;
    return fetch(url).then((res) => res.json());
  },
    getNotes(username) {
    username = username.toLowerCase().trim();
    var url = `https://console.firebase.google.com/u/0/project/githubnotes-f484b/database/githubnotes-f484b/data/${username}.json`;
    return fetch(url).then((res) => res.json());
   },
    addNotes(username) {
    username = username.toLowerCase().trim();
    var url = `https://console.firebase.google.com/u/0/project/githubnotes-f484b/database/githubnotes-f484b/data/${username}.json`;
    return fetch(url, {
         method: 'post',
         body: JSON.stringify(note),
    }).then((res) => res.json());
  }
};


export default api;