import React, { Component } from 'react';
import { WebView,
	      View,
	    StyleSheet
      } from 'react-native';
import PropTypes from 'prop-types';

export default class Web extends Component{
  render() {
    return (
      <View style={styles.container}>
      <WebView url={this.props.url} />
      </View>
    );
  }
}

Web.propTypes = {
	url: PropTypes.string.isRequired
};


const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F6F6EF',
		flexDirection: 'column'
	},
});